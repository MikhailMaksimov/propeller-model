# Propeller Model #

Model of a propeller design made with Blender.

A client asked me to make a model for a propeller with a specific wing shape and lots of perforations in the blades.
Model was redesigned several times to make the desired shape.
Sadly, I was not able to cut off parts of the blades that stick out inside the hub.
